package de.okinlab.measuremywall;

import android.util.Log;

import org.artoolkit.ar.base.rendering.RenderUtils;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.*;

/**
 * Shelf class provides all information about the Shelf properties
 */
public class Shelf {

    public boolean expertModeOn = false;

    private float totalHeight;
    private float totalWidth;
    private float totalDepth;

    public ArrayList<Float> expertData;

    public float[] shelfVertices;
    private byte shelfLineIndices[] = {
            //bottom
            0, 1,
            1, 2,
            2, 3,
            3, 0,
            //upward edges
            0, 4,
            1, 5,
            2, 6,
            3, 7,
            //top
            4, 5,
            5, 6,
            6, 7,
            7, 4
    };

    //TODO: ADD 2nd board for each vertical to make it visible from the other side
    private byte shelfOuterBoards[] = {
            0, 2, 1, 0, 3, 2,    //ground
            1, 6, 2, 1, 5, 6,    //right side
            0, 7, 3, 0, 4, 7,    //left side
            //3, 6, 2, 3, 7, 6,    //back side
            4, 6, 5, 4, 7, 6     //top
    };

    public ArrayList<Float> horizontalBoardVertices;
    public ArrayList<Byte> horizontalBoardIndices;

    public ArrayList<Float> verticalBoardVertices;
    private ArrayList<Byte> verticalBoardIndices;

    private int amountHBoards;
    private int amountVBoards;
    SimpleRenderer renderer;


    public Shelf(SimpleRenderer renderer) {
        this.shelfVertices = new float[24];
        this.amountHBoards = 0;
        this.amountVBoards = 0;

        this.renderer = renderer;

        this.horizontalBoardVertices = new ArrayList<>();
        this.horizontalBoardIndices = new ArrayList<>();

        this.verticalBoardVertices = new ArrayList<>();
        this.verticalBoardIndices = new ArrayList<>();
    }

    /**
     * Updates 3 Vertex data values for the shelf. This only affects the vertices of the shelf BBox. Not the vertices for the boards.
     *
     * @param startingIndex The fist index of the vertices to be updated. Check the index carefully.
     * @param values        An array of at least size 3 with the new vertex data
     */
    public void updateShelfVertex(int startingIndex, float[] values) {
        this.shelfVertices[startingIndex] = values[0];
        this.shelfVertices[startingIndex + 1] = values[1];
        this.shelfVertices[startingIndex + 2] = values[2];

        updateBoards();

        updateTotalMeasurements();
    }

    /**
     * Returns 3 coordinate data of a vertex point of the shelf bounding box.
     *
     * @param startingIndex Index of the vertex * 3
     * @return array with the 3 coordinates of the vertex
     */
    public float[] getShelfVertex(int startingIndex) {
        float[] result = new float[3];

        result[0] = this.shelfVertices[startingIndex];
        result[1] = this.shelfVertices[startingIndex + 1];
        result[2] = this.shelfVertices[startingIndex + 2];

        return result;
    }

    //Buffer Getters

    /**
     * Returns a buffer with all vertex information of:
     * - The shelf bounding box (index 0 - 7)
     * - The horizontal shelf boards (index 8 - amount HBoards * 4 - 1)
     * - The vertical shelf boards (index amount HBoards * 4  - amount VBoards * 4 - 1)
     *
     * @return Vertex buffer for the OpenGl Renderer
     */
    public FloatBuffer getShelfVertexBuffer() {
        //TODO: Cleanup needed
        ArrayList<Float> test = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            test.add(shelfVertices[i]);
        }

        test.addAll(horizontalBoardVertices);
        test.addAll(verticalBoardVertices);

        return RenderUtils.buildFloatBuffer(ListToArrayf(test));
    }

    /**
     * Returns the actual amount of horizontal shelf boards
     *
     * @return amount of horizontal shelf boards
     */
    public int getHBoardAmount() {
        return this.amountHBoards;
    }

    public void clearBoards(){
        this.horizontalBoardVertices.clear();
        this.horizontalBoardIndices.clear();
        this.amountHBoards = 0;

        this.verticalBoardVertices.clear();
        this.verticalBoardIndices.clear();
        this.amountVBoards = 0;
    }

    /**
     * Returns the actual amount of vertical shelf boards
     *
     * @return amount of vertical shelf boards
     */
    public int getVBoardAmount() {
        return this.amountVBoards;
    }

    public FloatBuffer getHorizontalBoardsVertexBuffer() {
        return RenderUtils.buildFloatBuffer(ListToArrayf(this.horizontalBoardVertices));
    }

    public FloatBuffer getVerticalBoardsVertexBuffer() {
        return RenderUtils.buildFloatBuffer(ListToArrayf(this.verticalBoardVertices));
    }

    public ByteBuffer getShelfLineIndicesBuffer() {
        return RenderUtils.buildByteBuffer(this.shelfLineIndices);
    }

    public ByteBuffer getShelfOuterIndicesBuffer() {
        return RenderUtils.buildByteBuffer(this.shelfOuterBoards);
    }

    public ByteBuffer getHorizontalBoardsIndicesBuffer() {
        return RenderUtils.buildByteBuffer(ListToArrayb(this.horizontalBoardIndices));
    }

    public ByteBuffer getVerticalBoardsIndicesBuffer() {
        return RenderUtils.buildByteBuffer(ListToArrayb(this.verticalBoardIndices));
    }

    public void updateBoards() {

        //vertical
        if (this.amountVBoards > 0) {

            // calculate divided right vector
            float[] rightVector = new float[3];
            float[] divRightVector = new float[3];

            subVectors(shelfVertices, 2 * 3, shelfVertices, 3 * 3, rightVector, 0);
            divVectorComp(rightVector, 0, this.amountVBoards + 1, divRightVector, 0);

            float[] actDivRight = new float[3];
            actDivRight[0] = divRightVector[0];
            actDivRight[1] = divRightVector[1];
            actDivRight[2] = divRightVector[2];

            //updating vertices vertices
            float[] results = new float[3];
            int newStartingIndex = 0;

            for (int i = 0; i < this.amountVBoards; i++) {

                //Vector next to point 0
                addVectors(shelfVertices, 0 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.set(0 + newStartingIndex, results[0]);
                verticalBoardVertices.set(1 + newStartingIndex, results[1]);
                verticalBoardVertices.set(2 + newStartingIndex, results[2]);

                //Vector next to point 3
                addVectors(shelfVertices, 3 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.set(3 + newStartingIndex, results[0]);
                verticalBoardVertices.set(4 + newStartingIndex, results[1]);
                verticalBoardVertices.set(5 + newStartingIndex, results[2]);

                //Vector next to point 7
                addVectors(shelfVertices, 7 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.set(6 + newStartingIndex, results[0]);
                verticalBoardVertices.set(7 + newStartingIndex, results[1]);
                verticalBoardVertices.set(8 + newStartingIndex, results[2]);

                //Vector next to point 4
                addVectors(shelfVertices, 4 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.set(9 + newStartingIndex, results[0]);
                verticalBoardVertices.set(10 + newStartingIndex, results[1]);
                verticalBoardVertices.set(11 + newStartingIndex, results[2]);

                //preparing the right vector for the next loop iteration
                actDivRight[0] += divRightVector[0];
                actDivRight[1] += divRightVector[1];
                actDivRight[2] += divRightVector[2];
                newStartingIndex += 12;
            }
        }

        //horizontal
        if (this.amountHBoards > 0) {

            // calculate divided up vector
            float[] heightVector = new float[3];
            float[] divHeightVector = new float[3];

            subVectors(shelfVertices, 6 * 3, shelfVertices, 2 * 3, heightVector, 0);
            divVectorComp(heightVector, 0, this.amountHBoards + 1, divHeightVector, 0);

            float[] actDivHeight = new float[3];
            actDivHeight[0] = divHeightVector[0];
            actDivHeight[1] = divHeightVector[1];
            actDivHeight[2] = divHeightVector[2];

            //updating vertices vertices
            float[] results = new float[3];
            int newStartingIndex = 0;

            for (int i = 0; i < this.amountHBoards; i++) {

                //Vector above point 0
                addVectors(shelfVertices, 0 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.set(0 + newStartingIndex, results[0]);
                horizontalBoardVertices.set(1 + newStartingIndex, results[1]);
                horizontalBoardVertices.set(2 + newStartingIndex, results[2]);

                //Vector above point 1
                addVectors(shelfVertices, 1 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.set(3 + newStartingIndex, results[0]);
                horizontalBoardVertices.set(4 + newStartingIndex, results[1]);
                horizontalBoardVertices.set(5 + newStartingIndex, results[2]);

                //Vector above point 2
                addVectors(shelfVertices, 2 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.set(6 + newStartingIndex, results[0]);
                horizontalBoardVertices.set(7 + newStartingIndex, results[1]);
                horizontalBoardVertices.set(8 + newStartingIndex, results[2]);

                //Vector above point 3
                addVectors(shelfVertices, 3 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.set(9 + newStartingIndex, results[0]);
                horizontalBoardVertices.set(10 + newStartingIndex, results[1]);
                horizontalBoardVertices.set(11 + newStartingIndex, results[2]);

                //Vector above point 3
                addVectors(shelfVertices, 3 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.set(9 + newStartingIndex, results[0]);
                horizontalBoardVertices.set(10 + newStartingIndex, results[1]);
                horizontalBoardVertices.set(11 + newStartingIndex, results[2]);

                //preparing the height vector for the next loop iteration
                actDivHeight[0] += divHeightVector[0];
                actDivHeight[1] += divHeightVector[1];
                actDivHeight[2] += divHeightVector[2];
                newStartingIndex += 12;
            }
        }
    }

    //TODO: Some Bugs are here after adding and removing boards. Need to be fixed soon
    public void remBoard(boolean vertical) {
        if (vertical) {
            this.amountVBoards -= 2;
            this.addBoard(true);
        } else {
            this.amountHBoards -= 2;
            this.addBoard(false);
        }
    }

    /**
     * Adds a vertical or horizontal shelf board to the shelf.
     * Caution!: Horizontal Boards must be added first
     *
     * @param vertical
     */
    public void addBoard(boolean vertical) {

        if (vertical) {
            verticalBoardVertices.clear();
            verticalBoardIndices.clear();

            //calculate divided right vector
            float[] rightVector = new float[3];
            float[] divRightVector = new float[3];

            //calculating right vector
            subVectors(shelfVertices, 2 * 3, shelfVertices, 3 * 3, rightVector, 0);
            divVectorComp(rightVector, 0, this.amountVBoards + 2, divRightVector, 0);

            float[] actDivRight = new float[3];
            actDivRight[0] = divRightVector[0];
            actDivRight[1] = divRightVector[1];
            actDivRight[2] = divRightVector[2];

            //creating and adding new vertices
            float[] results = new float[3];

            int newStartingIndex;

            if(this.expertModeOn){
                newStartingIndex = 8 + renderer.visibleBoards * 4;
            } else {
                newStartingIndex = 8 + this.amountHBoards * 4;
            }

            for (int i = 0; i < amountVBoards + 1; i++) {

                //Vector next to point 0
                addVectors(shelfVertices, 0 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.add(results[0]);
                verticalBoardVertices.add(results[1]);
                verticalBoardVertices.add(results[2]);

                //Vector next to point 3
                addVectors(shelfVertices, 3 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.add(results[0]);
                verticalBoardVertices.add(results[1]);
                verticalBoardVertices.add(results[2]);

                //Vector next to point 7
                addVectors(shelfVertices, 7 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.add(results[0]);
                verticalBoardVertices.add(results[1]);
                verticalBoardVertices.add(results[2]);

                //Vector next to point 4
                addVectors(shelfVertices, 4 * 3, actDivRight, 0, results, 0);
                verticalBoardVertices.add(results[0]);
                verticalBoardVertices.add(results[1]);
                verticalBoardVertices.add(results[2]);

                //preparing the right vectors for the next loop iteration
                actDivRight[0] += divRightVector[0];
                actDivRight[1] += divRightVector[1];
                actDivRight[2] += divRightVector[2];

                //Connecting vertices to build triangles
                verticalBoardIndices.add((byte) newStartingIndex);
                verticalBoardIndices.add((byte) (newStartingIndex + 2));
                verticalBoardIndices.add((byte) (newStartingIndex + 1));

                verticalBoardIndices.add((byte) newStartingIndex);
                verticalBoardIndices.add((byte) (newStartingIndex + 3));
                verticalBoardIndices.add((byte) (newStartingIndex + 2));

                //preparing Startingindex for next loop iteration
                newStartingIndex += 4;
            }

            this.amountVBoards++;

        } else { // horizontal board

            horizontalBoardVertices.clear();
            horizontalBoardIndices.clear();

            //calculate divided up vector
            float[] heightVector = new float[3];
            float[] divHeightVector = new float[3];

            subVectors(shelfVertices, 6 * 3, shelfVertices, 2 * 3, heightVector, 0);
            divVectorComp(heightVector, 0, this.amountHBoards + 2, divHeightVector, 0);

            float[] actDivHeight = new float[3];
            actDivHeight[0] = divHeightVector[0];
            actDivHeight[1] = divHeightVector[1];
            actDivHeight[2] = divHeightVector[2];

            //creating and adding new vertices
            float[] results = new float[3];

            //is always 8 because all horizontal board vertices come after the BBx vertices
            int newStartingIndex = 8;

            for (int i = 0; i < amountHBoards + 1; i++) {

                //Vector above point 0
                addVectors(shelfVertices, 0 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.add(results[0]);
                horizontalBoardVertices.add(results[1]);
                horizontalBoardVertices.add(results[2]);

                //Vector above point 1
                addVectors(shelfVertices, 1 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.add(results[0]);
                horizontalBoardVertices.add(results[1]);
                horizontalBoardVertices.add(results[2]);

                //Vector above point 2
                addVectors(shelfVertices, 2 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.add(results[0]);
                horizontalBoardVertices.add(results[1]);
                horizontalBoardVertices.add(results[2]);

                //Vector above point 3
                addVectors(shelfVertices, 3 * 3, actDivHeight, 0, results, 0);
                horizontalBoardVertices.add(results[0]);
                horizontalBoardVertices.add(results[1]);
                horizontalBoardVertices.add(results[2]);

                //preparing the height vector for the next loop iteration
                actDivHeight[0] += divHeightVector[0];
                actDivHeight[1] += divHeightVector[1];
                actDivHeight[2] += divHeightVector[2];

                //Connecting vertices to build triangles
                horizontalBoardIndices.add((byte) newStartingIndex);
                horizontalBoardIndices.add((byte) (newStartingIndex + 2));
                horizontalBoardIndices.add((byte) (newStartingIndex + 1));

                horizontalBoardIndices.add((byte) newStartingIndex);
                horizontalBoardIndices.add((byte) (newStartingIndex + 3));
                horizontalBoardIndices.add((byte) (newStartingIndex + 2));

                //preparing Startingindex for next loop iteration
                newStartingIndex += 4;
            }

            this.amountHBoards++;
        }
    }

    private void updateTotalMeasurements() {
        this.totalWidth = calcVectorDistance(shelfVertices, 3 * 3, shelfVertices, 2 * 3);
        this.totalDepth = calcVectorDistance(shelfVertices, 1 * 3, shelfVertices, 2 * 3);
        this.totalHeight = calcVectorDistance(shelfVertices, 2 * 3, shelfVertices, 6 * 3);
    }

    public float getTotalWidth() {
        return this.totalWidth;
    }

    public float getTotalHeight() {
        return this.totalHeight;
    }

    public float getTotalDepth() {
        return this.totalDepth;
    }


    protected static void subVectors(float[] aFlt_vectorA, int int_offsetA, float[] aFlt_vectorB, int int_offsetB, float[] aFlt_vectorOutput,
                                     int int_offsetOutput) {
        aFlt_vectorOutput[int_offsetOutput] = aFlt_vectorA[int_offsetA] - aFlt_vectorB[int_offsetB];
        aFlt_vectorOutput[int_offsetOutput + 1] = aFlt_vectorA[int_offsetA + 1] - aFlt_vectorB[int_offsetB + 1];
        aFlt_vectorOutput[int_offsetOutput + 2] = aFlt_vectorA[int_offsetA + 2] - aFlt_vectorB[int_offsetB + 2];
    }

    protected static void addVectors(float[] aFlt_vectorA, int int_offsetA, float[] aFlt_vectorB, int int_offsetB, float[] aFlt_vectorOutput,
                                     int int_offsetOutput) {
        aFlt_vectorOutput[int_offsetOutput] = aFlt_vectorA[int_offsetA] + aFlt_vectorB[int_offsetB];
        aFlt_vectorOutput[int_offsetOutput + 1] = aFlt_vectorA[int_offsetA + 1] + aFlt_vectorB[int_offsetB + 1];
        aFlt_vectorOutput[int_offsetOutput + 2] = aFlt_vectorA[int_offsetA + 2] + aFlt_vectorB[int_offsetB + 2];
    }

    protected static void divVectorComp(float[] aFlt_vectorA, int int_offsetA, float divVal, float[] aFlt_vectorOutput,
                                        int int_offsetOutput) {
        aFlt_vectorOutput[int_offsetOutput] = aFlt_vectorA[int_offsetA] / divVal;
        aFlt_vectorOutput[int_offsetOutput + 1] = aFlt_vectorA[int_offsetA + 1] / divVal;
        aFlt_vectorOutput[int_offsetOutput + 2] = aFlt_vectorA[int_offsetA + 2] / divVal;
    }


    protected static float calcVectorDistance(float[] aFlt_vectorA, int int_offsetA, float[] aFlt_vectorB, int int_offsetB) {
        return (float) Math.sqrt(Math.pow(aFlt_vectorA[int_offsetA] - aFlt_vectorB[int_offsetB], 2) +
                Math.pow(aFlt_vectorA[int_offsetA + 1] - aFlt_vectorB[int_offsetB + 1], 2) +
                Math.pow(aFlt_vectorA[int_offsetA + 2] - aFlt_vectorB[int_offsetB + 2], 2));
    }

    protected static void applyTransform(float[] vector, int vectorOffset, float[] matrix, int matrixOffset, float[] output, int outputOffset) {
        for (int i = 0; i < 3; i++) {
            output[outputOffset + i] = matrix[matrixOffset + i] * vector[vectorOffset] //
                    + matrix[matrixOffset + i + 4] * vector[vectorOffset + 1] //
                    + matrix[matrixOffset + i + 8] * vector[vectorOffset + 2] //
                    + matrix[matrixOffset + i + 12] * 1;
        }
    }

    /**
     * Converts a float list to a float array
     *
     * @param inputList List to be converted
     * @return float array with same content
     */
    public static float[] ListToArrayf(List<Float> inputList) {

        float[] result = new float[inputList.size()];
        int i = 0;

        for (Float f : inputList) {
            result[i] = (f != null ? f : Float.NaN);
            i++;
        }
        return result;
    }

    /**
     * Converts a byte list to a byte array
     *
     * @param inputList List to be converted
     * @return byte array with same content
     */
    public static byte[] ListToArrayb(List<Byte> inputList) {

        byte[] result = new byte[inputList.size()];
        int i = 0;

        for (Byte f : inputList) {
            result[i] = (f != null ? f : Byte.parseByte("NULL"));
            i++;
        }
        return result;
    }

    public static boolean invertMatrix(float[] input, float[] output) {

        float[] inv = new float[16];
        float det;

        int i;

        inv[0] = input[5] * input[10] * input[15] -
                input[5] * input[11] * input[14] -
                input[9] * input[6] * input[15] +
                input[9] * input[7] * input[14] +
                input[13] * input[6] * input[11] -
                input[13] * input[7] * input[10];

        inv[4] = -input[4] * input[10] * input[15] +
                input[4] * input[11] * input[14] +
                input[8] * input[6] * input[15] -
                input[8] * input[7] * input[14] -
                input[12] * input[6] * input[11] +
                input[12] * input[7] * input[10];

        inv[8] = input[4] * input[9] * input[15] -
                input[4] * input[11] * input[13] -
                input[8] * input[5] * input[15] +
                input[8] * input[7] * input[13] +
                input[12] * input[5] * input[11] -
                input[12] * input[7] * input[9];

        inv[12] = -input[4] * input[9] * input[14] +
                input[4] * input[10] * input[13] +
                input[8] * input[5] * input[14] -
                input[8] * input[6] * input[13] -
                input[12] * input[5] * input[10] +
                input[12] * input[6] * input[9];

        inv[1] = -input[1] * input[10] * input[15] +
                input[1] * input[11] * input[14] +
                input[9] * input[2] * input[15] -
                input[9] * input[3] * input[14] -
                input[13] * input[2] * input[11] +
                input[13] * input[3] * input[10];

        inv[5] = input[0] * input[10] * input[15] -
                input[0] * input[11] * input[14] -
                input[8] * input[2] * input[15] +
                input[8] * input[3] * input[14] +
                input[12] * input[2] * input[11] -
                input[12] * input[3] * input[10];

        inv[9] = -input[0] * input[9] * input[15] +
                input[0] * input[11] * input[13] +
                input[8] * input[1] * input[15] -
                input[8] * input[3] * input[13] -
                input[12] * input[1] * input[11] +
                input[12] * input[3] * input[9];

        inv[13] = input[0] * input[9] * input[14] -
                input[0] * input[10] * input[13] -
                input[8] * input[1] * input[14] +
                input[8] * input[2] * input[13] +
                input[12] * input[1] * input[10] -
                input[12] * input[2] * input[9];

        inv[2] = input[1] * input[6] * input[15] -
                input[1] * input[7] * input[14] -
                input[5] * input[2] * input[15] +
                input[5] * input[3] * input[14] +
                input[13] * input[2] * input[7] -
                input[13] * input[3] * input[6];

        inv[6] = -input[0] * input[6] * input[15] +
                input[0] * input[7] * input[14] +
                input[4] * input[2] * input[15] -
                input[4] * input[3] * input[14] -
                input[12] * input[2] * input[7] +
                input[12] * input[3] * input[6];

        inv[10] = input[0] * input[5] * input[15] -
                input[0] * input[7] * input[13] -
                input[4] * input[1] * input[15] +
                input[4] * input[3] * input[13] +
                input[12] * input[1] * input[7] -
                input[12] * input[3] * input[5];

        inv[14] = -input[0] * input[5] * input[14] +
                input[0] * input[6] * input[13] +
                input[4] * input[1] * input[14] -
                input[4] * input[2] * input[13] -
                input[12] * input[1] * input[6] +
                input[12] * input[2] * input[5];

        inv[3] = -input[1] * input[6] * input[11] +
                input[1] * input[7] * input[10] +
                input[5] * input[2] * input[11] -
                input[5] * input[3] * input[10] -
                input[9] * input[2] * input[7] +
                input[9] * input[3] * input[6];

        inv[7] = input[0] * input[6] * input[11] -
                input[0] * input[7] * input[10] -
                input[4] * input[2] * input[11] +
                input[4] * input[3] * input[10] +
                input[8] * input[2] * input[7] -
                input[8] * input[3] * input[6];

        inv[11] = -input[0] * input[5] * input[11] +
                input[0] * input[7] * input[9] +
                input[4] * input[1] * input[11] -
                input[4] * input[3] * input[9] -
                input[8] * input[1] * input[7] +
                input[8] * input[3] * input[5];

        inv[15] = input[0] * input[5] * input[10] -
                input[0] * input[6] * input[9] -
                input[4] * input[1] * input[10] +
                input[4] * input[2] * input[9] +
                input[8] * input[1] * input[6] -
                input[8] * input[2] * input[5];

        det = input[0] * inv[0] + input[1] * inv[4] + input[2] * inv[8] + input[3] * inv[12];

        if (det == 0)
            return false;

        det = 1.f / det;

        for (i = 0; i < 16; i++)
            output[i] = inv[i] * det;

        return true;
    }
}

