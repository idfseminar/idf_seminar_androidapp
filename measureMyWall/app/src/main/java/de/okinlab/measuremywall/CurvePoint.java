package de.okinlab.measuremywall;

/**
 * A Curve point represents a point on a Shelf board's edge. A Shelf board has at least two curve points
 */
public class CurvePoint {

    /**
     * The xValue of a curve is the relative to the Shelf boards width. Is in interval [0,1]
     */
    private float xValue;

    /**
     * The zValue of a curve is the absolute while 0 means that the point lies on the Shelf board's rectangle.
     */
    private float zValue;

    /**
     * A reference to the  belonging board
     */
    private ShelfBoard board;


    public CurvePoint(ShelfBoard board, float x, float z){

        this.board = board;
        this.xValue = x;
        this.zValue = z;
    }

    public float getXValue(){
        return this.xValue;
    }

    public float getZValue(){
        return this.zValue;
    }

    public float getAbsoluteXValue(){
        //TODO: Not implemented
        return 0;
    }

    public float getAbsoluteZValue(){
        //TODO: Not implemented
        return 0;
    }


}
