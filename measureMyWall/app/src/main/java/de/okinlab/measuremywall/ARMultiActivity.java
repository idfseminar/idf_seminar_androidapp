/*
 *  ARMultiActivity.java
 *  ARToolKit5
 *
 *  Disclaimer: IMPORTANT:  This Daqri software is supplied to you by Daqri
 *  LLC ("Daqri") in consideration of your agreement to the following
 *  terms, and your use, installation, modification or redistribution of
 *  this Daqri software constitutes acceptance of these terms.  If you do
 *  not agree with these terms, please do not use, install, modify or
 *  redistribute this Daqri software.
 *
 *  In consideration of your agreement to abide by the following terms, and
 *  subject to these terms, Daqri grants you a personal, non-exclusive
 *  license, under Daqri's copyrights in this original Daqri software (the
 *  "Daqri Software"), to use, reproduce, modify and redistribute the Daqri
 *  Software, with or without modifications, in source and/or binary forms;
 *  provided that if you redistribute the Daqri Software in its entirety and
 *  without modifications, you must retain this notice and the following
 *  text and disclaimers in all such redistributions of the Daqri Software.
 *  Neither the name, trademarks, service marks or logos of Daqri LLC may
 *  be used to endorse or promote products derived from the Daqri Software
 *  without specific prior written permission from Daqri.  Except as
 *  expressly stated in this notice, no other rights or licenses, express or
 *  implied, are granted by Daqri herein, including but not limited to any
 *  patent rights that may be infringed by your derivative works or by other
 *  works in which the Daqri Software may be incorporated.
 *
 *  The Daqri Software is provided by Daqri on an "AS IS" basis.  DAQRI
 *  MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 *  THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE, REGARDING THE DAQRI SOFTWARE OR ITS USE AND
 *  OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 *
 *  IN NO EVENT SHALL DAQRI BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 *  OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 *  MODIFICATION AND/OR DISTRIBUTION OF THE DAQRI SOFTWARE, HOWEVER CAUSED
 *  AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 *  STRICT LIABILITY OR OTHERWISE, EVEN IF DAQRI HAS BEEN ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  Copyright 2015 Daqri, LLC.
 *  Copyright 2011-2015 ARToolworks, Inc.
 *
 *  Author(s): Julian Looser, Philip Lamb
 *
 */

package de.okinlab.measuremywall;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import android.util.Log;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.artoolkit.ar.base.ARActivity;
import org.artoolkit.ar.base.rendering.ARRenderer;

import java.util.Locale;

/**
 * A very simple example of extending ARActivity to create a new AR application.
 */
public class ARMultiActivity extends ARActivity implements SimpleRenderer.SizeListener {
	private TextView tv_widthInfo;
	private TextView tv_depthInfo;
	private TextView tv_heightInfo;

	ImageButton addHorBtn;
	ImageButton remHorBtn;
	TextView horAmountText;

	ImageButton addVerBtn;
	ImageButton remVerBtn;
	ImageButton saveBtn;
	TextView verAmountText;

	Switch modeSwitch;

	protected Handler obj_handler = new Handler();
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;

	//Starting point of application
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);



		//Getting information about phone screen settings
		tv_widthInfo = (TextView) findViewById(R.id.tv_widthInfo);
		tv_depthInfo = (TextView) findViewById(R.id.tv_depthInfo);
		tv_heightInfo = (TextView) findViewById(R.id.tv_heightInfo);
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

		//Setting button functions for side menu
		addHorBtn = (ImageButton) findViewById(R.id.addHorizontalButton);
		remHorBtn = (ImageButton) findViewById(R.id.remHorizontalButton);
		horAmountText = (TextView) findViewById(R.id.horizontalAmountText);

		addVerBtn = (ImageButton) findViewById(R.id.addVerticalButton);
		remVerBtn = (ImageButton) findViewById(R.id.remVerticalButton);
		verAmountText = (TextView) findViewById(R.id.verticalAmountText);

		saveBtn = (ImageButton) findViewById(R.id.saveBtn);

		modeSwitch = (Switch) findViewById(R.id.modeSwitch);
	}

	/**
	 * Provide our own SimpleRenderer.
	 */
	@Override
	protected ARRenderer supplyRenderer() {
		return new SimpleRenderer(this);
	}

	/**
	 * Use the FrameLayout in this Activity's UI.
	 */
	@Override
	protected FrameLayout supplyFrameLayout() {
		return (FrameLayout) this.findViewById(R.id.mainLayout);
	}

	@Override
	public void notifySize(final float flt_width, final float flt_depth, final float flt_height) {
		obj_handler.post(new Runnable() {
			@Override
			public void run() {
				tv_widthInfo.setText(String.format(Locale.US, "Width: %.3f m", flt_width / 1000));
				tv_depthInfo.setText(String.format(Locale.US, "Depth: %.3f m", flt_depth / 1000));
				tv_heightInfo.setText(String.format(Locale.US, "Height: %.3f m", flt_height / 1000));
			}

		});
	}

	public void addAllListeners(final Shelf actShelf) {

		//Add horizontal Button
		addHorBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				actShelf.addBoard(false);
				horAmountText.setText("" + actShelf.getHBoardAmount());
			}
		});

		//Remove horizontal Button
		remHorBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				actShelf.remBoard(false);
				horAmountText.setText("" + actShelf.getHBoardAmount());
			}
		});

		//Add vertical - Button
		addVerBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				actShelf.addBoard(true);
				verAmountText.setText("" + actShelf.getVBoardAmount());
			}
		});

		//Remove vertical - Button
		remVerBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				actShelf.remBoard(true);
				verAmountText.setText("" + actShelf.getVBoardAmount());
			}
		});

		//save JSON data - Button
		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				DataExporter exporter = new DataExporter(actShelf);
				exporter.exportJSON();
			}
		});

		//Mode Switch
		modeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					actShelf.expertModeOn = true;
					findViewById(R.id.horizontalMenu).setVisibility(View.INVISIBLE);
					Toast.makeText(getApplicationContext(), "Expert mode on", Toast.LENGTH_SHORT).show();
				}else{
					actShelf.expertModeOn = false;
					findViewById(R.id.horizontalMenu).setVisibility(View.VISIBLE);
					Toast.makeText(getApplicationContext(), "Expert mode off", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"ARMulti Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this auto-generated app deep link URI is correct.
				Uri.parse("android-app://de.okinlab.measuremywall/http/host/path")
		);
		AppIndex.AppIndexApi.start(client, viewAction);
	}

	@Override
	public void onStop() {
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"ARMulti Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this auto-generated app deep link URI is correct.
				Uri.parse("android-app://de.okinlab.measuremywall/http/host/path")
		);
		AppIndex.AppIndexApi.end(client, viewAction);
		client.disconnect();
	}
}