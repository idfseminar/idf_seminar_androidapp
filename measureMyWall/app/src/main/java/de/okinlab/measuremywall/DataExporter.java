package de.okinlab.measuremywall;

/**
 * Created by marker on 19/1/16.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataExporter {
    private Shelf shelf_object;
    String tag = "DataExporter:";
    public DataExporter(Shelf obj) {
        this.shelf_object = obj;
    }

    private String getJsonData() throws JSONException {
        JSONObject root_obj = new JSONObject();
        JSONObject bbox_obj = new JSONObject();
        JSONArray hor_shelfs = new JSONArray();

        bbox_obj.put("height", shelf_object.getTotalHeight());
        bbox_obj.put("depth", shelf_object.getTotalDepth());
        bbox_obj.put("width", shelf_object.getTotalWidth());

        root_obj.put("shelf", bbox_obj);
        root_obj.put("verticals", shelf_object.getVBoardAmount());

        if (!shelf_object.expertModeOn) {

            for (int i = 0; i < shelf_object.getHBoardAmount(); i++){
             JSONObject h = new JSONObject();

                //non-expert mode - Just interpolate
                float height = (shelf_object.getTotalHeight() / (shelf_object.getHBoardAmount() + 1)) * (i + 1);
                h.put("height", height);
                h.put("width", shelf_object.getTotalWidth());
                h.put("offset", 0);
                hor_shelfs.put(i, h);
            }

            root_obj.put("horizontals", hor_shelfs);
            return root_obj.toString();
        } else {
            int index = 0;
            for (int i = 0; i < shelf_object.expertData.size() / 3; i++ ){
                JSONObject h = new JSONObject();

                h.put("height", shelf_object.expertData.get(index + 2));
                h.put("width", shelf_object.expertData.get(index));
                h.put("offset", shelf_object.expertData.get(index + 1));

                index+= 3;
                hor_shelfs.put(i, h);
            }
            root_obj.put("horizontals", hor_shelfs);
            return root_obj.toString();
            }
    }

    public void exportJSON() {
        String filename = "data.json";
        FileOutputStream outputStream;

        try {
            File root = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOWNLOADS) +
                        "/okinlab");

            if (!root.exists()) {
                root.mkdirs();
                Log.d(tag, "Creating ... " + root.toString());
            }

            File file = new File(root, filename);
            outputStream = new FileOutputStream(file, false);
            outputStream.write(getJsonData().getBytes());
            Log.d(tag, "Writing file " + filename);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
