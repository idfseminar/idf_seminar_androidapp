package de.okinlab.measuremywall;

import java.util.*;

/**
 * Vertical or horizontal Shelf boards.
 */
public class ShelfBoard {

    private float[] depthVec;
    private float[] leftUpVec;
    private float[] rightUpVec;

    private ArrayList<Float> boardVertices;
    private ArrayList<Byte> boardIndices;

    public ShelfBoard(){
        this.boardVertices = new ArrayList<>();
        this.boardIndices = new ArrayList<>();

        //Adding vertices fot the triangles
        this.boardIndices.add((byte) 0);
        this.boardIndices.add((byte) 2);
        this.boardIndices.add((byte) 1);

        this.boardIndices.add((byte) 0);
        this.boardIndices.add((byte) 3);
        this.boardIndices.add((byte) 2);
    }

    public void updateBoardVertices(int startingIndex, float[] values){
        this.boardVertices.set(startingIndex, values[0]);
        this.boardVertices.set(1 + startingIndex, values[1]);
        this.boardVertices.set(2 + startingIndex, values[2]);
    }

    public ArrayList<Float> getBoardVertices(){
        return this.boardVertices;
    }

    public ArrayList<Byte> getBoardIndices(int startingIndex){
        ArrayList<Byte> resultList = new ArrayList<>();
        for (int i = 0; i < this.boardIndices.size(); i++){
            int actValue = this.boardIndices.get(i);
            resultList.add((byte) (actValue + startingIndex));
        }
        return this.boardIndices;
    }




}
