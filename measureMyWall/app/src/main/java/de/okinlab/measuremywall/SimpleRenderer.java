/*
 *  SimpleRenderer.java
 *  ARToolKit5
 *
 *  Disclaimer: IMPORTANT:  This Daqri software is supplied to you by Daqri
 *  LLC ("Daqri") in consideration of your agreement to the following
 *  terms, and your use, installation, modification or redistribution of
 *  this Daqri software constitutes acceptance of these terms.  If you do
 *  not agree with these terms, please do not use, install, modify or
 *  redistribute this Daqri software.
 *
 *  In consideration of your agreement to abide by the following terms, and
 *  subject to these terms, Daqri grants you a personal, non-exclusive
 *  license, under Daqri's copyrights in this original Daqri software (the
 *  "Daqri Software"), to use, reproduce, modify and redistribute the Daqri
 *  Software, with or without modifications, in source and/or binary forms;
 *  provided that if you redistribute the Daqri Software in its entirety and
 *  without modifications, you must retain this notice and the following
 *  text and disclaimers in all such redistributions of the Daqri Software.
 *  Neither the name, trademarks, service marks or logos of Daqri LLC may
 *  be used to endorse or promote products derived from the Daqri Software
 *  without specific prior written permission from Daqri.  Except as
 *  expressly stated in this notice, no other rights or licenses, express or
 *  implied, are granted by Daqri herein, including but not limited to any
 *  patent rights that may be infringed by your derivative works or by other
 *  works in which the Daqri Software may be incorporated.
 *
 *  The Daqri Software is provided by Daqri on an "AS IS" basis.  DAQRI
 *  MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 *  THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE, REGARDING THE DAQRI SOFTWARE OR ITS USE AND
 *  OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 *
 *  IN NO EVENT SHALL DAQRI BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 *  OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 *  MODIFICATION AND/OR DISTRIBUTION OF THE DAQRI SOFTWARE, HOWEVER CAUSED
 *  AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 *  STRICT LIABILITY OR OTHERWISE, EVEN IF DAQRI HAS BEEN ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  Copyright 2015 Daqri, LLC.
 *  Copyright 2011-2015 ARToolworks, Inc.
 *
 *  Author(s): Julian Looser, Philip Lamb
 *
 */

package de.okinlab.measuremywall;

import android.opengl.GLES10;
import android.util.Log;
import javax.microedition.khronos.opengles.GL10;
import org.artoolkit.ar.base.ARToolKit;
import org.artoolkit.ar.base.NativeInterface;
import org.artoolkit.ar.base.rendering.ARRenderer;
import org.artoolkit.ar.base.rendering.Cube;
import org.artoolkit.ar.base.rendering.RenderUtils;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;

/**
 * A very simple Renderer that adds a marker and draws a cube on it.
 */
public class SimpleRenderer extends ARRenderer
{
	public interface SizeListener
	{
		 void notifySize( float flt_width, float flt_depth, float flt_height );
	}

	public SimpleRenderer( SizeListener obj_listener )
	{
		this.obj_sizeListener = obj_listener;
		this.mainShelf = new Shelf(this);

		ARMultiActivity test = (ARMultiActivity) obj_sizeListener;
		test.addAllListeners(this.mainShelf);
	}

	private Shelf mainShelf;

	//initializing the markers for the BBox cube
	private int markerID_A = -1;
	private int markerID_B = -1;
	private int markerID_C = -1;
	private int markerID_D = -1;
	private int markerID_F = -1;

	//initializing the markers for the expert mode
	private int markerID_alpha_l = -1;
	private int markerID_alpha_r = -1;
	private int markerID_beta_l = -1;
	private int markerID_beta_r = -1;
	private int markerID_gamma_l = -1;
	private int markerID_gamma_r = -1;
	private int markerID_delta_l = -1;
	private int markerID_delta_r = -1;

	ByteBuffer HorBoardsIndexBuffer;
	public int visibleBoards = 0;

	//printed marker size = 120mm default
	private static final int int_markerSize = 120;

	private SizeListener obj_sizeListener = null;

	//Coordinates of the marker positions (fitting to edges)
	private float aFlt_markerUpperLeftCorner[] = { -int_markerSize / 2, int_markerSize / 2, .0f };
	private float aFlt_markerUpperRightCorner[] = { int_markerSize / 2, int_markerSize / 2, .0f };
	private float aFlt_markerLowerLeftCorner[] = { -int_markerSize / 2, -int_markerSize / 2, .0f };
	private float aFlt_markerLowerRightCorner[] = { int_markerSize / 2, -int_markerSize / 2, .0f };

	private float aFlt_markerLeftEdge[] = { -int_markerSize / 2, .0f, .0f };
	private float aFlt_markerRightEdge[] = { int_markerSize / 2, .0f, .0f };

	public ArrayList<Float> expHorBoardInfo = new ArrayList<>();

	float c = 1.0f;

	// colors for rendering the cube
	float colors[] = { 0, 0, 0, c, // 0 black
		255, 0, 0, c, // 1 red
		255, 255, 0, c, // 2 yellow
		0, 255, 0, c, // 3 green
		0, 0, 255, c, // 4 blue
		255, 0, 255, c, // 5 magenta
		255, 255, 255, c, // 6 white
		0, 255, 255, c, // 7 cyan
	};

	private FloatBuffer mVertexBuffer;
	private FloatBuffer mColorBuffer;
	private ByteBuffer mIndexBuffer;

	private ByteBuffer mHorizontalIndexBuffer;
	private ByteBuffer mVerticalIndexBuffer;

	private Cube obj_testCube = new Cube( int_markerSize );
	private int frameCount = 0;

	/**
	 * Markers can be configured here.
	 */
	@Override
	public boolean configureARScene()
	{
		final ARToolKit obj_arInstance = ARToolKit.getInstance();
		//Setting up the use of the 64 2D markers you can find on "ARToolKit5-bin-5.3.1-Android\doc\patterns\Matrix code 3x3"
		NativeInterface.arwSetPatternDetectionMode(NativeInterface.AR_MATRIX_CODE_DETECTION);
		NativeInterface.arwSetMatrixCodeType(NativeInterface.AR_MATRIX_CODE_3x3_PARITY65);

        //Add marker for A
		markerID_A = obj_arInstance.addMarker( "single_barcode;3;" + int_markerSize );
		if( markerID_A < 0 )
		{
			return false;
		}

        //Add marker for B
		markerID_B = obj_arInstance.addMarker( "single_barcode;25;" + int_markerSize );
		if( markerID_B < 0 )
		{
			return false;
		}

        //Add marker for C
		markerID_C = obj_arInstance.addMarker( "single_barcode;28;" + int_markerSize );
		if( markerID_C < 0 )
		{
			return false;
		}

        //Add marker for D
		markerID_D = obj_arInstance.addMarker( "single_barcode;26;" + int_markerSize );
		if( markerID_D < 0 )
		{
			return false;
		}

        //Add marker for F
		markerID_F = obj_arInstance.addMarker("single_barcode;16;" + int_markerSize);
		if( markerID_F < 0 )
		{
			return false;
		}


		//EXPERT MODE MARKERS

		//Add marker for alpha_l
		markerID_alpha_l = obj_arInstance.addMarker("single_barcode;7;" + int_markerSize);
		if( markerID_alpha_l < 0 )
		{
			return false;
		}

		//Add marker for alpha_r
		markerID_alpha_r = obj_arInstance.addMarker("single_barcode;8;" + int_markerSize);
		if( markerID_alpha_r < 0 )
		{
			return false;
		}

		//Add marker for beta_l
		markerID_beta_l = obj_arInstance.addMarker("single_barcode;9;" + int_markerSize);
		if( markerID_beta_l < 0 )
		{
			return false;
		}

		//Add marker for beta_r
		markerID_beta_r = obj_arInstance.addMarker("single_barcode;10;" + int_markerSize);
		if( markerID_beta_r < 0 )
		{
			return false;
		}

		//Add marker for gamma_l
		markerID_gamma_l = obj_arInstance.addMarker("single_barcode;11;" + int_markerSize);
		if( markerID_gamma_l < 0 )
		{
			return false;
		}

		//Add marker for gamma_r
		markerID_gamma_r = obj_arInstance.addMarker("single_barcode;12;" + int_markerSize);
		if( markerID_gamma_r < 0 )
		{
			return false;
		}

		//Add marker for delta_l
		markerID_delta_l = obj_arInstance.addMarker("single_barcode;13;" + int_markerSize);
		if( markerID_delta_l < 0 )
		{
			return false;
		}

		//Add marker for delta_r
		markerID_delta_r = obj_arInstance.addMarker("single_barcode;14;" + int_markerSize);
		if( markerID_delta_r < 0 )
		{
			return false;
		}

		return true;
	}

	/**
	 * Override the draw function from ARRenderer.
     * Will be called every camera image update (~30fps)
	 */
	@Override
	public void draw( GL10 gl ) {
		final ARToolKit obj_arInstance = ARToolKit.getInstance();

		boolean is_markerAVisible = obj_arInstance.queryMarkerVisible(markerID_A);
		boolean is_markerBVisible = obj_arInstance.queryMarkerVisible(markerID_B);
		boolean is_markerCVisible = obj_arInstance.queryMarkerVisible(markerID_C);
		boolean is_markerDVisible = obj_arInstance.queryMarkerVisible(markerID_D);
		boolean is_markerFVisible = obj_arInstance.queryMarkerVisible(markerID_F);


		boolean drawMesh = true;

			//computing all values
			if (is_markerFVisible) {
				Log.d("f visible:" , "true");
				// If all markers visible, calc Boundaries and draw a cube and fill the coordinates into vertices[]
				if (is_markerAVisible && is_markerBVisible && is_markerCVisible && is_markerDVisible) {

					Log.d("all visible:" , "true");
					float[] aFlt_markerFUpperRight = new float[3];
					float[] aFlt_heightVector = new float[3];
					float[] aFlt_width = new float[3];

					float[] matrix = obj_arInstance.queryMarkerTransformation(markerID_F);
					applyTransform(aFlt_markerUpperRightCorner, 0, matrix, 0, aFlt_markerFUpperRight, 0);

					float[] helpArray = new float[3];

					//get position of marker B
					matrix = obj_arInstance.queryMarkerTransformation(markerID_B);
					applyTransform(aFlt_markerUpperRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(2 * 3, helpArray);

					//calculate the vector between marker B and marker F which setups the height of the bbox
					subVectors(aFlt_markerFUpperRight, 0, mainShelf.getShelfVertex(2 * 3), 0, aFlt_heightVector, 0);

					//Adding height Vector to B to save position for F ? Why because F = makerUpperRightCorner?
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(2 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(6 * 3, helpArray);

					// get Transformation Matrix for marker A
					matrix = obj_arInstance.queryMarkerTransformation(markerID_A);
					applyTransform(aFlt_markerUpperLeftCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(3 * 3, helpArray);

					//getWidthVector
					subVectors(mainShelf.getShelfVertex(0 * 3), 0, mainShelf.getShelfVertex(3 * 3), 0, aFlt_width, 0);

					//add the height Vector to get the top Upper Left of the Cube
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(3 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(7 * 3, helpArray);

					matrix = obj_arInstance.queryMarkerTransformation(markerID_C);
					applyTransform(aFlt_markerLowerRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(1 * 3, helpArray);

					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(1 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(5 * 3, helpArray);

					matrix = obj_arInstance.queryMarkerTransformation(markerID_D);
					applyTransform(aFlt_markerLowerLeftCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(0 * 3, helpArray);

					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(0 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(4 * 3, helpArray);

					mIndexBuffer = mainShelf.getShelfLineIndicesBuffer();

					//building buffers
					if (!mainShelf.expertModeOn) {
						mVertexBuffer = mainShelf.getShelfVertexBuffer();
					} else {
						//EXPERT-Mode on
						expHorBoardInfo.clear();
						visibleBoards = 0;

						ArrayList<Float> horBoardVertices = new ArrayList<>();
						ArrayList<Byte> horBoardIndices = new ArrayList<>();

						boolean is_markerAlphaVisible = obj_arInstance.queryMarkerVisible(markerID_alpha_l) && obj_arInstance.queryMarkerVisible(markerID_alpha_r);
						boolean is_markerBetaVisible = obj_arInstance.queryMarkerVisible(markerID_beta_l) && obj_arInstance.queryMarkerVisible(markerID_beta_r);
						boolean is_markerGammaVisible = obj_arInstance.queryMarkerVisible(markerID_gamma_l) && obj_arInstance.queryMarkerVisible(markerID_gamma_r);
						boolean is_markerDeltaVisible = obj_arInstance.queryMarkerVisible(markerID_delta_l) && obj_arInstance.queryMarkerVisible(markerID_delta_r);

						if (is_markerAlphaVisible){
							Log.d("ALPHA:" , "detected");
						} else {
							Log.d("ALPHA:" , "not detected");
						}


						int startingIndex = 8;

						float[] centralMiddle = new float[3];
						subVectors(mainShelf.getShelfVertex(2 * 3),0,mainShelf.getShelfVertex(3 * 3) , 0, centralMiddle, 0);
						divVectorComp(centralMiddle, 0, 2, centralMiddle, 0);
						addVectors(mainShelf.getShelfVertex(3 * 3), 0, centralMiddle, 0, centralMiddle, 0);

						if (is_markerAlphaVisible) {

							float[] alpha = new float[12];

							matrix = obj_arInstance.queryMarkerTransformation(markerID_alpha_l);
							applyTransform(aFlt_markerLeftEdge, 0, matrix, 0, alpha, 3 * 3);

							matrix = obj_arInstance.queryMarkerTransformation(markerID_alpha_r);
							applyTransform(aFlt_markerRightEdge, 0, matrix, 0, alpha, 2 * 3);

							addVectors(alpha, 3 * 3, aFlt_width, 0, alpha, 0 * 3);
							addVectors(alpha, 2 * 3, aFlt_width, 0, alpha, 1 * 3);

							for (int i = 0; i < 12; i++) {
								horBoardVertices.add(alpha[i]);
							}

							//compute width of board
							float actBWidth = calcVectorDistance(alpha, 3 * 3, alpha, 2 * 3);
							expHorBoardInfo.add(actBWidth);

							//compute offset
							float[] middlePoint = new float[3];
							subVectors(alpha, 2 * 3, alpha, 3 * 3, middlePoint, 0);
							divVectorComp(middlePoint, 0, 2, middlePoint, 0);
							addVectors(alpha, 3 * 3, middlePoint, 0, middlePoint, 0);

							float[] hyp = new float[3];
							float[] ank = new float[3];

							subVectors(middlePoint, 0, mainShelf.getShelfVertex(3 * 3), 0, hyp, 0);
							normalizeVector(hyp, 0, hyp, 0);

							subVectors(mainShelf.getShelfVertex(2 * 3), 0, mainShelf.getShelfVertex(3 * 3), 0, ank, 0);
							normalizeVector(ank, 0, ank, 0);

							float cosVal = (float) Math.cos(getAngle(hyp, 0, ank, 0));
							ank[0] = cosVal * ank[0];
							ank[1] = cosVal * ank[1];
							ank[2] = cosVal * ank[2];

							float[] offsetPoint = new float[3];
							addVectors(mainShelf.getShelfVertex(3 * 3), 0, ank, 0, offsetPoint, 0);

							float finalOffset = calcVectorDistance(offsetPoint, 0, centralMiddle, 0);
							expHorBoardInfo.add(finalOffset);

							//compute height of board
							float height = calcVectorDistance(offsetPoint, 0, middlePoint, 0);
							expHorBoardInfo.add(height);

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 2));
							horBoardIndices.add((byte) (startingIndex + 1));

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 3));
							horBoardIndices.add((byte) (startingIndex + 2));

							startingIndex += 4;
							visibleBoards++;
						}

						if (is_markerBetaVisible) {

							float[] beta = new float[12];

							matrix = obj_arInstance.queryMarkerTransformation(markerID_beta_l);
							applyTransform(aFlt_markerLeftEdge, 0, matrix, 0, beta, 3 * 3);

							matrix = obj_arInstance.queryMarkerTransformation(markerID_beta_r);
							applyTransform(aFlt_markerRightEdge, 0, matrix, 0, beta, 2 * 3);

							addVectors(beta, 3 * 3, aFlt_width, 0, beta, 0 * 3);
							addVectors(beta, 2 * 3, aFlt_width, 0, beta, 1 * 3);

							for (int i = 0; i < 12; i++) {
								horBoardVertices.add(beta[i]);
							}

							//compute width of board
							float actBWidth = calcVectorDistance(beta, 3 * 3, beta, 2 * 3);
							expHorBoardInfo.add(actBWidth);

							//compute offset
							float[] middlePoint = new float[3];
							subVectors(beta, 2 * 3, beta, 3 * 3, middlePoint, 0);
							divVectorComp(middlePoint, 0, 2, middlePoint, 0);
							addVectors(beta, 3 * 3, middlePoint, 0, middlePoint, 0);

							float[] hyp = new float[3];
							float[] ank = new float[3];

							subVectors(middlePoint, 0, mainShelf.getShelfVertex(3 * 3), 0, hyp, 0);
							normalizeVector(hyp, 0, hyp, 0);

							subVectors(mainShelf.getShelfVertex(2 * 3), 0, mainShelf.getShelfVertex(3 * 3), 0, ank, 0);
							normalizeVector(ank, 0, ank, 0);

							float cosVal = (float) Math.cos(getAngle(hyp, 0, ank, 0));
							ank[0] = cosVal * ank[0];
							ank[1] = cosVal * ank[1];
							ank[2] = cosVal * ank[2];

							float[] offsetPoint = new float[3];
							addVectors(mainShelf.getShelfVertex(3 * 3), 0, ank, 0, offsetPoint, 0);

							float finalOffset = calcVectorDistance(offsetPoint, 0, centralMiddle, 0);
							expHorBoardInfo.add(finalOffset);

							//compute height of board
							float height = calcVectorDistance(offsetPoint, 0, middlePoint, 0);
							expHorBoardInfo.add(height);

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 2));
							horBoardIndices.add((byte) (startingIndex + 1));

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 3));
							horBoardIndices.add((byte) (startingIndex + 2));

							startingIndex += 4;
							visibleBoards++;
						}

						if (is_markerGammaVisible) {

							float[] gamma = new float[12];

							matrix = obj_arInstance.queryMarkerTransformation(markerID_gamma_l);
							applyTransform(aFlt_markerLeftEdge, 0, matrix, 0, gamma, 3 * 3);

							matrix = obj_arInstance.queryMarkerTransformation(markerID_gamma_r);
							applyTransform(aFlt_markerRightEdge, 0, matrix, 0, gamma, 2 * 3);

							addVectors(gamma, 3 * 3, aFlt_width, 0, gamma, 0 * 3);
							addVectors(gamma, 2 * 3, aFlt_width, 0, gamma, 1 * 3);

							for (int i = 0; i < 12; i++) {
								horBoardVertices.add(gamma[i]);
							}

							//compute width of board
							float actBWidth = calcVectorDistance(gamma, 3 * 3, gamma, 2 * 3);
							expHorBoardInfo.add(actBWidth);

							//compute offset
							float[] middlePoint = new float[3];
							subVectors(gamma, 2 * 3, gamma, 3 * 3, middlePoint, 0);
							divVectorComp(middlePoint, 0, 2, middlePoint, 0);
							addVectors(gamma, 3 * 3, middlePoint, 0, middlePoint, 0);

							float[] hyp = new float[3];
							float[] ank = new float[3];

							subVectors(middlePoint, 0, mainShelf.getShelfVertex(3 * 3), 0, hyp, 0);
							normalizeVector(hyp, 0, hyp, 0);

							subVectors(mainShelf.getShelfVertex(2 * 3), 0, mainShelf.getShelfVertex(3 * 3), 0, ank, 0);
							normalizeVector(ank, 0, ank, 0);

							float cosVal = (float) Math.cos(getAngle(hyp, 0, ank, 0));
							ank[0] = cosVal * ank[0];
							ank[1] = cosVal * ank[1];
							ank[2] = cosVal * ank[2];

							float[] offsetPoint = new float[3];
							addVectors(mainShelf.getShelfVertex(3 * 3), 0, ank, 0, offsetPoint, 0);

							float finalOffset = calcVectorDistance(offsetPoint, 0, centralMiddle, 0);
							expHorBoardInfo.add(finalOffset);

							//compute height of board
							float height = calcVectorDistance(offsetPoint, 0, middlePoint, 0);
							expHorBoardInfo.add(height);

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 2));
							horBoardIndices.add((byte) (startingIndex + 1));

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 3));
							horBoardIndices.add((byte) (startingIndex + 2));

							startingIndex += 4;
							visibleBoards++;
						}

						if (is_markerDeltaVisible) {

							float[] delta = new float[12];

							matrix = obj_arInstance.queryMarkerTransformation(markerID_delta_l);
							applyTransform(aFlt_markerLeftEdge, 0, matrix, 0, delta, 3 * 3);

							matrix = obj_arInstance.queryMarkerTransformation(markerID_delta_r);
							applyTransform(aFlt_markerRightEdge, 0, matrix, 0, delta, 2 * 3);

							addVectors(delta, 3 * 3, aFlt_width, 0, delta, 0 * 3);
							addVectors(delta, 2 * 3, aFlt_width, 0, delta, 1 * 3);

							for (int i = 0; i < 12; i++) {
								horBoardVertices.add(delta[i]);
							}

							//compute width of board
							float actBWidth = calcVectorDistance(delta, 3 * 3, delta, 2 * 3);
							expHorBoardInfo.add(actBWidth);

							//compute offset
							float[] middlePoint = new float[3];
							subVectors(delta, 2 * 3, delta, 3 * 3, middlePoint, 0);
							divVectorComp(middlePoint, 0, 2, middlePoint, 0);
							addVectors(delta, 3 * 3, middlePoint, 0, middlePoint, 0);

							float[] hyp = new float[3];
							float[] ank = new float[3];

							subVectors(middlePoint, 0, mainShelf.getShelfVertex(3 * 3), 0, hyp, 0);
							normalizeVector(hyp, 0, hyp, 0);

							subVectors(mainShelf.getShelfVertex(2 * 3), 0, mainShelf.getShelfVertex(3 * 3), 0, ank, 0);
							normalizeVector(ank, 0, ank, 0);

							float cosVal = (float) Math.cos(getAngle(hyp, 0, ank, 0));
							ank[0] = cosVal * ank[0];
							ank[1] = cosVal * ank[1];
							ank[2] = cosVal * ank[2];

							float[] offsetPoint = new float[3];
							addVectors(mainShelf.getShelfVertex(3 * 3), 0, ank, 0, offsetPoint, 0);

							float finalOffset = calcVectorDistance(offsetPoint, 0, centralMiddle, 0);
							expHorBoardInfo.add(finalOffset);

							//compute height of board
							float height = calcVectorDistance(offsetPoint, 0, middlePoint, 0);
							expHorBoardInfo.add(height);

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 2));
							horBoardIndices.add((byte) (startingIndex + 1));

							horBoardIndices.add((byte) (startingIndex + 0));
							horBoardIndices.add((byte) (startingIndex + 3));
							horBoardIndices.add((byte) (startingIndex + 2));

							visibleBoards++;
						}

						ArrayList<Float> mVertices = new ArrayList<>();

						for (int i = 0; i < 24; i++) {
							mVertices.add(mainShelf.shelfVertices[i]);
						}

						mVertices.addAll(horBoardVertices);
						mVertices.addAll(mainShelf.verticalBoardVertices);

						mVertexBuffer = RenderUtils.buildFloatBuffer(Shelf.ListToArrayf(mVertices));
						HorBoardsIndexBuffer = RenderUtils.buildByteBuffer(Shelf.ListToArrayb(horBoardIndices));
					}

					mainShelf.expertData = expHorBoardInfo;


					////Width, Height and Depth of the BBox display on screen
					float flt_width = mainShelf.getTotalWidth();
					float flt_depth = mainShelf.getTotalDepth();
					float flt_height = mainShelf.getTotalHeight();

					updateMeasureInfo(flt_width, flt_depth, flt_height);

				}//Alternative calcualtion, when not all markers are visible.
				else if( is_markerAVisible && is_markerBVisible && is_markerCVisible ) //  marker D not visible
				{
					float[] matrix = obj_arInstance.queryMarkerTransformation( markerID_F );
					float[] aFlt_markerFUpperRight = new float[ 3 ];
					applyTransform( aFlt_markerUpperRightCorner, 0, matrix, 0, aFlt_markerFUpperRight, 0 );
//
					float[] helpArray = new float[3];
//
					matrix = obj_arInstance.queryMarkerTransformation( markerID_B );
					applyTransform(aFlt_markerUpperRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(2 * 3, helpArray);
//////
					//calculate the vector between marker B and marker F
					float[] aFlt_heightVector = new float[ 3 ];
					subVectors(aFlt_markerFUpperRight, 0, mainShelf.getShelfVertex(2 * 3), 0, aFlt_heightVector, 0);
//////
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(2 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(6 * 3, helpArray);
//////
					// get Transformation Matrix for marker A
					matrix = obj_arInstance.queryMarkerTransformation( markerID_A );
					applyTransform(aFlt_markerUpperLeftCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(3 * 3, helpArray);
//
//
					//add the height Vector to get the top Upper Left of the Cube
					addVectors( aFlt_heightVector, 0, mainShelf.getShelfVertex(3 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(7 * 3, helpArray);
//////
					matrix = obj_arInstance.queryMarkerTransformation( markerID_C );
					applyTransform(aFlt_markerLowerRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(1 * 3, helpArray);
//
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(1 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(5 * 3, helpArray);
//////
					subVectors(mainShelf.getShelfVertex(3 * 3), 0, mainShelf.getShelfVertex(2 * 3), 0, aFlt_markerFUpperRight, 0);
					addVectors(mainShelf.getShelfVertex(1 * 3), 0, aFlt_markerFUpperRight, 0, helpArray, 0);
					mainShelf.updateShelfVertex(0 * 3, helpArray);
//////
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(0 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(4 * 3, helpArray);
//////
					mVertexBuffer = mainShelf.getShelfVertexBuffer();
					mIndexBuffer = mainShelf.getShelfLineIndicesBuffer();
//
					//Width, Height and Depth of the BBox display on screen
					float flt_width = mainShelf.getTotalWidth();
					float flt_depth = mainShelf.getTotalDepth();
					float flt_height = mainShelf.getTotalHeight();
//
					updateMeasureInfo(flt_width, flt_depth, flt_height);
				}
				else if( is_markerBVisible && is_markerCVisible && is_markerDVisible ) //  marker A not visible
				{
					float[] matrix = obj_arInstance.queryMarkerTransformation( markerID_F );
					float[] aFlt_markerFUpperRight = new float[ 3 ];
					applyTransform( aFlt_markerUpperRightCorner, 0, matrix, 0, aFlt_markerFUpperRight, 0 );
//////
					float[] helpArray = new float[3];
//
					matrix = obj_arInstance.queryMarkerTransformation( markerID_B );
					applyTransform( aFlt_markerUpperRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(2 * 3, helpArray);
//////
					//calculate the vector between marker B and marker F
					float[] aFlt_heightVector = new float[ 3 ];
					subVectors(aFlt_markerFUpperRight, 0, mainShelf.getShelfVertex(2 * 3), 0, aFlt_heightVector, 0);
//////
					addVectors( aFlt_heightVector, 0, mainShelf.getShelfVertex(2 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(6 * 3, helpArray);
//////
					matrix = obj_arInstance.queryMarkerTransformation( markerID_C );
					applyTransform(aFlt_markerLowerRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(1 * 3, helpArray);
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(1 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(5 * 3, helpArray);
//////
					//calc D
					matrix = obj_arInstance.queryMarkerTransformation( markerID_D );
					applyTransform(aFlt_markerLowerLeftCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(0 * 3, helpArray);
//
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(0 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(4 * 3, helpArray);
//////
					//recalc A = B + (D-C)
					subVectors(mainShelf.getShelfVertex(0 * 3), 0, mainShelf.getShelfVertex(1 * 3), 0, aFlt_markerFUpperRight, 0);
					addVectors(mainShelf.getShelfVertex(2 * 3), 0, aFlt_markerFUpperRight, 0, helpArray, 0);
					mainShelf.updateShelfVertex(3 * 3, helpArray);
//////
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(3 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(7 * 3, helpArray);
//////
					mVertexBuffer = mainShelf.getShelfVertexBuffer();
					mIndexBuffer = mainShelf.getShelfLineIndicesBuffer();
//
					//Width, Height and Depth of the BBox display on screen
					float flt_width = mainShelf.getTotalWidth();
					float flt_depth = mainShelf.getTotalDepth();
					float flt_height = mainShelf.getTotalHeight();
//
					updateMeasureInfo(flt_width, flt_depth, flt_height);
//////
				}
				else if( is_markerAVisible && is_markerBVisible && is_markerDVisible ) //  marker C not visible
				{
					float[] matrix = obj_arInstance.queryMarkerTransformation( markerID_F );
					float[] aFlt_markerFUpperRight = new float[ 3 ];
					applyTransform( aFlt_markerUpperRightCorner, 0, matrix, 0, aFlt_markerFUpperRight, 0 );
//////
					float[] helpArray = new float[3];
//
					matrix = obj_arInstance.queryMarkerTransformation( markerID_B );
					applyTransform( aFlt_markerUpperRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(2 * 3, helpArray);
//////
					//calculate the vector between marker B and marker F
					float[] aFlt_heightVector = new float[ 3 ];
					subVectors(aFlt_markerFUpperRight, 0, mainShelf.getShelfVertex(2 * 3), 0, aFlt_heightVector, 0);
//////
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(2 * 3), 0,helpArray, 0);
					mainShelf.updateShelfVertex(6 * 3, helpArray);
//////
					// get Transformation Matrix for marker A
					matrix = obj_arInstance.queryMarkerTransformation( markerID_A );
					//apply Transformation to the Upper Left Corner to get the bottom Upper Left of the Cube
					applyTransform(aFlt_markerUpperLeftCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(3 * 3, helpArray);
					//add the height Vector to get the top Upper Left of the Cube
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(3 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(7 * 3, helpArray);
//////
					//calc D
					matrix = obj_arInstance.queryMarkerTransformation( markerID_D );
					applyTransform(aFlt_markerLowerLeftCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(0 * 3, helpArray);
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(0 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(4* 3, helpArray);
//////
					//recalc C = D + (B - A)
					subVectors(mainShelf.getShelfVertex(2 * 3), 0, mainShelf.getShelfVertex(3 * 3), 0, aFlt_markerFUpperRight, 0);
					addVectors(mainShelf.getShelfVertex(0 * 3), 0, aFlt_markerFUpperRight, 0, helpArray, 0);
					mainShelf.updateShelfVertex(1 * 3, helpArray);
//////
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(1 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(5 * 3, helpArray);
//////
					mVertexBuffer = mainShelf.getShelfVertexBuffer();
					mIndexBuffer = mainShelf.getShelfLineIndicesBuffer();
//
					//Width, Height and Depth of the BBox display on screen
					float flt_width = mainShelf.getTotalWidth();
					float flt_depth = mainShelf.getTotalDepth();
					float flt_height = mainShelf.getTotalHeight();
//
					updateMeasureInfo(flt_width, flt_depth, flt_height);
//////
				}
				else if( is_markerAVisible && is_markerCVisible && is_markerDVisible ) // marker B not visible
				{
					float[] helpArray = new float[3];
//
					// get Position marker A
					float[] matrix = obj_arInstance.queryMarkerTransformation( markerID_A );
					applyTransform( aFlt_markerUpperLeftCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(3 * 3, helpArray);
//////
					// get Position marker C
					matrix = obj_arInstance.queryMarkerTransformation( markerID_C );
					applyTransform( aFlt_markerLowerRightCorner, 0, matrix, 0, helpArray, 0);
					mainShelf.updateShelfVertex(1 * 3, helpArray);
//////
					// get Position marker D
					matrix = obj_arInstance.queryMarkerTransformation( markerID_D );
					applyTransform( aFlt_markerLowerLeftCorner, 0, matrix, 0,helpArray, 0);
					mainShelf.updateShelfVertex(0 * 3, helpArray);
//////
					//recalc B = A + (C - D)
					float[] aFlt_markerFUpperRight = new float[ 3 ];
					subVectors( mainShelf.getShelfVertex(1 * 3), 0, mainShelf.getShelfVertex(0 * 3), 0, aFlt_markerFUpperRight, 0 );
					addVectors(mainShelf.getShelfVertex(3 * 3), 0, aFlt_markerFUpperRight, 0, helpArray, 0);
					mainShelf.updateShelfVertex(2 * 3, helpArray);
//////
					// get Position marker F
					matrix = obj_arInstance.queryMarkerTransformation( markerID_F );
					applyTransform(aFlt_markerUpperRightCorner, 0, matrix, 0, aFlt_markerFUpperRight, 0);
//////
					//calculate the vector between marker B and marker F
					float[] aFlt_heightVector = new float[ 3 ];
					subVectors(aFlt_markerFUpperRight, 0, mainShelf.getShelfVertex(2 * 3), 0, aFlt_heightVector, 0);
//////
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(3 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(7 * 3, helpArray);
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(2 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(6 * 3, helpArray);
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(1 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(5 * 3, helpArray);
					addVectors(aFlt_heightVector, 0, mainShelf.getShelfVertex(0 * 3), 0, helpArray, 0);
					mainShelf.updateShelfVertex(4 * 3, helpArray);
//////
					mVertexBuffer = mainShelf.getShelfVertexBuffer();
					mIndexBuffer = mainShelf.getShelfLineIndicesBuffer();
//
					//Width, Height and Depth of the BBox display on screen
					float flt_width = mainShelf.getTotalWidth();
					float flt_depth = mainShelf.getTotalDepth();
					float flt_height = mainShelf.getTotalHeight();
//
					updateMeasureInfo(flt_width, flt_depth, flt_height);
//////
				}
			else //if too less markers are visible TODO: Maybe add more possible missing markers
				{
					drawMesh = false;
				}
			} else //if marker F (upper left top) is not visible
			{
				drawMesh = false;
			}

			//here actually start drawing
		if (drawMesh) {
			gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

			// Apply the ARToolKit projection matrix
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadMatrixf(obj_arInstance.getProjectionMatrix(), 0);

			//cure appearance settings
			gl.glEnable(GL10.GL_CULL_FACE);
			gl.glShadeModel(GL10.GL_SMOOTH);
			gl.glEnable(GL10.GL_DEPTH_TEST);
			gl.glFrontFace(GL10.GL_CW);

			//WTF why setting new Float??
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadMatrixf(new float[]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}, 0);

			mColorBuffer = RenderUtils.buildFloatBuffer(colors);


			GLES10.glColorPointer(4, GLES10.GL_FLOAT, 0, mColorBuffer);
			GLES10.glVertexPointer(3, GLES10.GL_FLOAT, 0, mVertexBuffer);
			GLES10.glEnableClientState(GLES10.GL_COLOR_ARRAY);
			GLES10.glEnableClientState(GLES10.GL_VERTEX_ARRAY);
			GLES10.glLineWidth(10.f);



			if(!mainShelf.expertModeOn) {

				Log.d("draw Lines:", " reached");
				//drawing BBox lines
				GLES10.glDrawElements(GLES10.GL_LINES, 24, GLES10.GL_UNSIGNED_BYTE, mIndexBuffer);

				//draw vertical boards
				if (mainShelf.getVBoardAmount() > 0) {
					mVerticalIndexBuffer = mainShelf.getVerticalBoardsIndicesBuffer();
					GLES10.glDrawElements(GLES10.GL_TRIANGLES, mainShelf.getVBoardAmount() * 6, GLES10.GL_UNSIGNED_BYTE, mVerticalIndexBuffer);
				}

				//draw horizontal boards
				if (mainShelf.getHBoardAmount() > 0) {
					mHorizontalIndexBuffer = mainShelf.getHorizontalBoardsIndicesBuffer();
					GLES10.glDrawElements(GLES10.GL_TRIANGLES, mainShelf.getHBoardAmount() * 6, GLES10.GL_UNSIGNED_BYTE, mHorizontalIndexBuffer);
				}

				//draw outer boards
				ByteBuffer outerIndexBuffer = mainShelf.getShelfOuterIndicesBuffer();
				GLES10.glDrawElements(GLES10.GL_TRIANGLES, 24, GLES10.GL_UNSIGNED_BYTE, outerIndexBuffer);

			} else {    //Expert mode

				if (visibleBoards > 0) {
					Log.d("draw horizontals:", " reached");
					Log.d("visible boards:", " " + visibleBoards);
					GLES10.glDrawElements(GLES10.GL_TRIANGLES, visibleBoards * 6, GLES10.GL_UNSIGNED_BYTE, HorBoardsIndexBuffer);
					//GLES10.glDrawElements(GLES10.GL_LINES, visibleBoards * 6, GLES10.GL_UNSIGNED_BYTE, HorBoardsIndexBuffer);
				}


				//draw vertical boards
				if (mainShelf.getVBoardAmount() > 0) {
					Log.d("draw verticals:", " reached");
					mVerticalIndexBuffer = mainShelf.getVerticalBoardsIndicesBuffer();
					GLES10.glDrawElements(GLES10.GL_TRIANGLES, mainShelf.getVBoardAmount() * 6, GLES10.GL_UNSIGNED_BYTE, mVerticalIndexBuffer);
				}

				//draw outer boards
				ByteBuffer outerIndexBuffer = mainShelf.getShelfOuterIndicesBuffer();
				GLES10.glDrawElements(GLES10.GL_TRIANGLES, 24, GLES10.GL_UNSIGNED_BYTE, outerIndexBuffer);
			}

				GLES10.glDisableClientState(GLES10.GL_COLOR_ARRAY);
				GLES10.glDisableClientState(GLES10.GL_VERTEX_ARRAY);


			} else //if not all markers are visible, only draw the local cubes at each marker
			 {
				frameCount = (frameCount + 1) % 2;
				if (frameCount != 0) {
					return;
				}

				 gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

				// Apply the ARToolKit projection matrix
				gl.glMatrixMode(GL10.GL_PROJECTION);
				gl.glLoadMatrixf(obj_arInstance.getProjectionMatrix(), 0);

				 gl.glEnable(GL10.GL_CULL_FACE);
				gl.glShadeModel(GL10.GL_SMOOTH);
				 gl.glEnable(GL10.GL_DEPTH_TEST);
				gl.glFrontFace(GL10.GL_CW);


				//draw all detected markers
				if (obj_arInstance.queryMarkerVisible(markerID_A)) {
					float[] matrix = obj_arInstance.queryMarkerTransformation(markerID_A);
					drawTestCubes(gl, matrix);
				}
				if (obj_arInstance.queryMarkerVisible(markerID_B)) {
					float[] matrix = obj_arInstance.queryMarkerTransformation(markerID_B);
					drawTestCubes(gl, matrix);
				}
				if (obj_arInstance.queryMarkerVisible(markerID_C)) {
					float[] matrix = obj_arInstance.queryMarkerTransformation(markerID_C);
					drawTestCubes(gl, matrix);
				}
				if (obj_arInstance.queryMarkerVisible(markerID_D)) {
					float[] matrix = obj_arInstance.queryMarkerTransformation(markerID_D);
					drawTestCubes(gl, matrix);
				}
				if (obj_arInstance.queryMarkerVisible(markerID_F)) {
					float[] matrix = obj_arInstance.queryMarkerTransformation(markerID_F);
					drawTestCubes(gl, matrix);
				}

				if(mainShelf.expertModeOn) {
					//tryDrawTestCube(obj_arInstance, gl, markerID_A);
					//tryDrawTestCube(obj_arInstance, gl, markerID_D);
					tryDrawTestCube(obj_arInstance, gl, markerID_alpha_l);
					tryDrawTestCube(obj_arInstance, gl, markerID_alpha_r);
					tryDrawTestCube(obj_arInstance, gl, markerID_beta_l);
					tryDrawTestCube(obj_arInstance, gl, markerID_beta_r);
					tryDrawTestCube(obj_arInstance, gl, markerID_gamma_l);
					tryDrawTestCube(obj_arInstance, gl, markerID_gamma_r);
					tryDrawTestCube(obj_arInstance, gl, markerID_delta_l);
					tryDrawTestCube(obj_arInstance, gl, markerID_delta_r);
				}
			}
		}


	public void drawTestCubes( GL10 gl, float[] matrix )
	{
		gl.glMatrixMode( GL10.GL_MODELVIEW );
		gl.glLoadMatrixf( matrix, 0 );
		obj_testCube.draw( gl );
	}


	protected static void subVectors( float[] aFlt_vectorA, int int_offsetA, float[] aFlt_vectorB, int int_offsetB, float[] aFlt_vectorOutput,
									  int int_offsetOutput )
	{
		aFlt_vectorOutput[ int_offsetOutput ] = aFlt_vectorA[ int_offsetA ] - aFlt_vectorB[ int_offsetB ];
		aFlt_vectorOutput[ int_offsetOutput + 1 ] = aFlt_vectorA[ int_offsetA + 1 ] - aFlt_vectorB[ int_offsetB + 1 ];
		aFlt_vectorOutput[ int_offsetOutput + 2 ] = aFlt_vectorA[ int_offsetA + 2 ] - aFlt_vectorB[ int_offsetB + 2 ];
	}

	protected static void addVectors( float[] aFlt_vectorA, int int_offsetA, float[] aFlt_vectorB, int int_offsetB, float[] aFlt_vectorOutput,
									  int int_offsetOutput )
	{
		aFlt_vectorOutput[ int_offsetOutput ] = aFlt_vectorA[ int_offsetA ] + aFlt_vectorB[ int_offsetB ];
		aFlt_vectorOutput[ int_offsetOutput + 1 ] = aFlt_vectorA[ int_offsetA + 1 ] + aFlt_vectorB[ int_offsetB + 1 ];
		aFlt_vectorOutput[ int_offsetOutput + 2 ] = aFlt_vectorA[ int_offsetA + 2 ] + aFlt_vectorB[ int_offsetB + 2 ];
	}

	protected static void divVectorComp( float[] aFlt_vectorA, int int_offsetA, float divVal, float[] aFlt_vectorOutput,
									  int int_offsetOutput )
	{
		aFlt_vectorOutput[ int_offsetOutput ] = aFlt_vectorA[ int_offsetA ] / divVal;
		aFlt_vectorOutput[ int_offsetOutput + 1 ] = aFlt_vectorA[ int_offsetA + 1 ] / divVal;
		aFlt_vectorOutput[ int_offsetOutput + 2 ] = aFlt_vectorA[ int_offsetA + 2 ] / divVal;
	}


	protected static float calcVectorDistance( float[] aFlt_vectorA, int int_offsetA, float[] aFlt_vectorB, int int_offsetB )
	{
		return ( float ) Math.sqrt( Math.pow( aFlt_vectorA[ int_offsetA ] - aFlt_vectorB[ int_offsetB ], 2 ) +
			Math.pow( aFlt_vectorA[ int_offsetA + 1 ] - aFlt_vectorB[ int_offsetB + 1 ], 2 ) +
			Math.pow( aFlt_vectorA[ int_offsetA + 2 ] - aFlt_vectorB[ int_offsetB + 2 ], 2 ) );
	}

	protected static void normalizeVector(float[] aFlt_vectorA, int int_offsetA, float[] output, int output_offset ){
		float length = (float) Math.sqrt(Math.pow(aFlt_vectorA[int_offsetA], 2) + Math.pow(aFlt_vectorA[int_offsetA + 1], 2) + Math.pow(aFlt_vectorA[int_offsetA + 2], 2));

		output[output_offset] = aFlt_vectorA[int_offsetA] / length;
		output[output_offset + 1] = aFlt_vectorA[int_offsetA + 1] / length;
		output[output_offset + 2] = aFlt_vectorA[int_offsetA + 2] / length;
	}

	protected static float getAngle(float[] aFlt_vectorA, int int_offsetA, float[] aFlt_vectorB, int int_offsetB){
		float[] norm1 = new float[3];
		float[] norm2 = new float[3];

		normalizeVector(aFlt_vectorA, int_offsetA, norm1, 0);
		normalizeVector(aFlt_vectorB, int_offsetB, norm2, 0);

		return norm1[0] * norm2[0] + norm1[1] * norm2[1] + norm1[2] * norm2[2];
	}

	protected static void applyTransform( float[] vector, int vectorOffset, float[] matrix, int matrixOffset, float[] output, int outputOffset )
	{
		for( int i = 0; i < 3; i++ )
		{
			output[ outputOffset + i ] = matrix[ matrixOffset + i ] * vector[ vectorOffset ] //
				+ matrix[ matrixOffset + i + 4 ] * vector[ vectorOffset + 1 ] //
				+ matrix[ matrixOffset + i + 8 ] * vector[ vectorOffset + 2 ] //
				+ matrix[ matrixOffset + i + 12 ] * 1;
		}
	}

	private void tryDrawTestCube(ARToolKit instance, GL10 gl, int marker_id){
		if (instance.queryMarkerVisible(marker_id)) {
			float[] matrix = instance.queryMarkerTransformation(marker_id);
			drawTestCubes(gl, matrix);
		}
	}

	/**
	 * Display width, depth and height information an app screen
	 * @param flt_width board width
	 * @param flt_depth board depth
	 * @param flt_height board height
	 */
	private void updateMeasureInfo(float flt_width, float flt_depth, float flt_height){

		if( obj_sizeListener != null )
		{
			obj_sizeListener.notifySize( flt_width, flt_depth, flt_height);
		}
	}
}
